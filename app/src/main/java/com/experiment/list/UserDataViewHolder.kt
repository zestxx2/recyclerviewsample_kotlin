package com.experiment.list

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class UserDataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(userData: UserData) {
        val nameView = itemView.findViewById<TextView>(R.id.name)
        val phoneNumberView = itemView.findViewById<TextView>(R.id.phone_number)

        nameView.text = userData.userName
        phoneNumberView.text = userData.userNumber
    }
}
