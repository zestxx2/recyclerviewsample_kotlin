package com.experiment.list

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Получаем экземпляр RecyclerView по id
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        //Создаем adapter
        val sampleListAdapter = SampleListAdapter(this, getFakeDataList())
        //Создаем простой LayoutManager
        val linearLayoutManager = LinearLayoutManager(this)
        //Передаем значения RecyclerView
        recyclerView.adapter = sampleListAdapter
        recyclerView.layoutManager = linearLayoutManager
    }

    fun getFakeDataList(): List<UserData> {
        return listOf(
            UserData("Иван", "+79559944998"),
            UserData("Игорь", "+79653454998"),
            UserData("Саша", "+79559244998"),
            UserData("Инга", "+79552344998"),
            UserData("Оля", "+79555674998"),
            UserData("Рома", "+79559454998"),
            UserData("Тема", "+79554644998"),
            UserData("Иван", "+79555564998")
        )
    }
}
